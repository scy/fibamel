from ..subprocess import call, json_call
from ..units import Bitrate, Dimensions


class AdaptiveQuality:
    def __init__(self, dimensions, vrate, arate):
        self.dimensions = Dimensions.parse(dimensions)
        self.vrate = Bitrate.parse(vrate)
        self.arate = Bitrate.parse(arate)
        self.asrate = 48_000

    def __str__(self):
        return '{0} @ {1}/{2}'.format(self.dimensions, self.vrate, self.arate)

    def hls_stanza(self, file_prefix, keyint=48, seglen=4):
        w, h = self.dimensions.x, self.dimensions.y
        return [
            '-vf', 'scale=-2:{0}:force_original_aspect_ratio=decrease'.format(
                h),
            '-c:a', 'aac', '-ar', self.asrate,
            '-b:a', str(self.arate),
            '-c:v', 'h264', '-profile:v', 'main',
            '-b:v', str(self.vrate), '-maxrate', int(self.vrate.rate * 1.1),
            '-bufsize', int(self.vrate.rate * 2),
            '-crf', 20,
            '-g', keyint, '-keyint_min', keyint, '-sc_threshold', 0,
            '-hls_segment_filename', '{0}_hls/{1}p_%04d.ts'.format(
                file_prefix, h),
            '{0}_hls/{1}p.m3u8'.format(file_prefix, h),
        ]


DEFAULT_ADAPTIVE = [
    AdaptiveQuality('3840x2160', '40m', '384k'),
    AdaptiveQuality('2560x1440', '16m', '256k'),
    AdaptiveQuality('1920x1080',  '8m', '192k'),
    AdaptiveQuality('1280x720',   '5m', '128k'),
    AdaptiveQuality('854x480',    '3m', '128k'),
    AdaptiveQuality('640x360',    '1m',  '96k'),
    AdaptiveQuality('426x240',    '1m',  '64k'),
]


def probe(input_file, ffprobe='ffprobe'):
    cmd = [ffprobe, '-show_format', '-show_streams', '-of', 'json', input_file]
    return json_call(cmd)


def max_video_dimensions(input_file):
    return max([Dimensions(s['width'], s['height'])
        for s in probe(input_file)['streams']
        if s['codec_type'] == 'video'])


def adaptive_formats(input_file, formats=DEFAULT_ADAPTIVE):
    formats = sorted(formats, key=lambda f: f.dimensions)
    target = max_video_dimensions(input_file)
    sensible = []
    last = False
    for format in formats:
        if format.dimensions < target or last:
            sensible.append(format)
            if last:
                break
        else:
            last = True
    return sensible


def to_hls(input_file, ffmpeg='ffmpeg'):
    cmd = [ffmpeg, '-i', input_file]
    for f in adaptive_formats(input_file):
        cmd.extend(f.hls_stanza(input_file))
    return call(cmd)
