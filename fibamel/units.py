from functools import total_ordering
import re


class Bitrate:
    SUFFIXES = [('m', 1_000_000), ('k', 1_000)]

    def __init__(self, rate):
        if isinstance(rate, int):
            self.rate = rate
        elif isinstance(rate, str):
            match = re.fullmatch(r'([0-9]+)([km])?', rate)
            if not match:
                raise ValueError('invalid bitrate: {0}'.format(rate))
            self.rate = int(match[1])
            for suffix, factor in self.SUFFIXES:
                if suffix == match[2]:
                    self.rate = self.rate * factor
                    break
        else:
            raise ValueError('bitrate value should be int, got {0}'.format(
                type(rate).__name__))

    def __str__(self):
        for suffix, factor in self.SUFFIXES:
            if self.rate % factor == 0:
                return str(self.rate // factor) + suffix
        return str(self.rate)

    @classmethod
    def parse(cls, value):
        return value if isinstance(value, cls) else cls(value)


@total_ordering
class Dimensions:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            raise NotImplemented
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        return self.y < other.y if self.y != other.y else self.x < other.x

    def __str__(self):
        return '{0}x{1}'.format(self.x, self.y)

    @classmethod
    def parse(cls, value):
        if isinstance(value, cls):
            return value
        if isinstance(value, tuple):
            if len(value) != 2:
                raise ValueError(
                    'dimension tuples should have 2 values, got {0}'.format(
                        len(value)))
            return cls(*value)
        if isinstance(value, str):
            match = re.fullmatch(r'([0-9]+)x([0-9]+)', value)
            if match:
                return cls(match[1], match[2])
        raise ValueError('dimension value not understood: {0}'.format(value))
