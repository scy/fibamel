import json as json_
import subprocess


class Error(Exception):
    def __init__(self, cmd, stdout=None, stderr=None):
        super().__init__('command failed: {0}'.format(cmd))
        self.stdout = stdout
        self.stderr = stderr


def call(args, input=None, timeout=None):
    print(args)
    args = [str(arg) for arg in args]
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate(input, timeout)
    if p.returncode != 0:
        print(out)
        print(err)
        raise Error(args, out, err)
    return out, err


def json_call(args, input=None, timeout=None):
    out, err = call(args, input, timeout)
    return json_.loads(out.decode('utf-8'))
