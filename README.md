# fibamel: file-based media library

This is _very_ much work in progress right now.

It’s supposed to become a tool that runs over your local photo/video files and folders and exports them to share with others.
These exports can be to a web server (which means that thumbnails have to be created and videos have to be converted in web-friendly formats and multiple bitrates) or another disk or folder.

## Status

Experimental video conversion of a single video to Apple’s HLS (using ffmpeg) can be performed by calling `fibamel.encode.ffmpeg.to_hls()`.
There’s no CLI yet.
Error handling is minimal, there’s no progress information, and (at least my version of) ffmpeg fails to scale to multiples of 2.

I’m planning to create tasks and milestones in [the Codeberg project](https://codeberg.org/scy/fibamel) that represent some kind of roadmap.
However, I don’t have much time to work on this.
Expect rough edges and small feature increments.

## Documentation

There’s a [FAQ](docs/faq.md) and some more [resources](docs/resources.md) in the `docs` folder.
