# Frequently Asked Questions

(Okay, to be fair, most of these have never been asked.)

## Video support

### Why are you using Apple HLS and not MPEG-DASH?

HLS has wider support in browsers.
As usual, Apple refuses to integrate DASH into their mobile browsers, for example.

That said, I like DASH more and would love to add support for it in a future version.

### Why aren’t you using ffmpeg-python?

[ffmpeg-python](https://github.com/kkroening/ffmpeg-python) is a library that’s supposed to make creating ffmpeg command lines easier.
However, it actually makes it _harder_ to follow tutorials on the web, because you’ll have to convert their command lines back to Python method calls.
Also, it’s using `-filter_complex` for all of the filtering, which looks substantially different and even has side effects compared to using regular filter arguments, making it even harder to borrow stuff from tutorials.

The thing I found most useful in ffmpeg-python was that I thought it parsed `ffprobe`’s output and converts it to JSON—but then I learned that that’s simply the effect of using the `-of json` flag.
