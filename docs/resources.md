# Collection of resources and links

## Apple’s HTTP Live Streaming (HLS)

* [Creating A Production Ready Multi Bitrate HLS VOD stream](https://docs.peer5.com/guides/production-ready-hls-vod/) (creates the master playlist manually, although ffmpeg should be capable to do this, too)
